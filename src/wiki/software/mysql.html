<h1>MySQL</h1>

<p>This article will take you through the installation and usage of MySQL, a database management system which underpins many other pieces of software. For the most part no expertise is required to use the guide. Some areas will require being able to <a href="/wiki/slots/ssh">connect to your slot via SSH</a> and you will be instructed to connect via SSH where this is the case.</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#installation">Installation</a></li>
            <li><a href="#details">Getting your MySQL details</a></li>
            <li><a href="#start">Starting, stopping and restarting</a></li>
            <li><a href="#usage">Using MySQL</a>
                <ul>
                    <li><a href="#connecting">Connecting to the MySQL monitor</a></li>
                    <li><a href="#basic-ops">Basic operations</a></li>
                    <li><a href="#backing-up">Backing up databases</a></li>
                </ul>
            </li>
            <li><a href="#uninstallation">Uninstallation</a></li>
            <li><a href="#external-links">External links</a></li>
        </ol>
    </nav>
</details> 

<h2 id="installation">Installation</h2>
<p>It is very simple to install or reinstall MySQL as the Feral web manager supports its installation. Please follow the separate guide to <a href="/wiki/getting-started/web-manager#install">installing software from the web manager</a>, selecting MySQL from the list.</p>

<h2 id="details">Getting your MySQL details</h2>
<p>Once the installation has been completed your Software page (accessible via link to the left-hand side of the Feral website) will display the socket, username and password.</p>
<aside class="alert note">If you're on a slot from before 2017, double-check the socket is correct as the Software page may be missing <samp>home</samp>. Check the path to your home directory by logging in via SSH and running <kbd>pwd</kbd>.</aside>

<h2 id="start">Starting, stopping and restarting</h2>
<p>As part of the initial installation process MySQL will also be started up for you. Every five minutes the system will scan the processes running and if MySQL is not running it will attempt to start it up. You can still control the process manually though, as below:</p>
<dl>
    <dt>start</dt>
    <dd><kbd>screen -S mysql -fa -d -m mysqld_safe --defaults-file=~/private/mysql/my.conf<kbd></dd>
    <dt>check running</dt>
    <dd><kbd>pgrep -fu "$(whoami)" 'mysql'</kbd></dd>
    <dt>stop</dt>
    <dd><kbd>pkill -fu "$(whoami)" 'mysql'</kbd></dd>
    <dt>restart</dt>
    <dd><kbd>pkill -fu "$(whoami)" 'mysql' &amp;&amp; sleep 3 &amp;&amp; screen -S mysql -fa -d -m mysqld_safe --defaults-file=~/private/mysql/my.conf</kbd></dd>
    <dt>kill (force stop)</dt>
    <dd><kbd>pkill -9 -fu "$(whoami)" 'mysql'</kbd></dd>
</dl>
<p>If you get any errors running these commands please check the troubleshooting section.</p>
<p>The <samp>check running</samp> command will return a process number (or three) if MySQL is running. If it doesn't return anything, MySQL is not running.</p>

<h2 id="usage">Using MySQL</h2>
<p>This section provides information on using MySQL via the command line. You may prefer using <a href="phpmyadmin">PHPMyAdmin</a> if you prefer a web interface.</p>

<h3 id="connecting">Connecting to the MySQL monitor</h3>
<p>Connect to your slot via SSH then use the following command (changing the variables) to connect:</p>
<p><kbd>mysql --socket=<var>path-to-socket</var> -p<var>password</var></kbd></p>
<p>Both the <var>path-to-socket</var> and <var>password</var> will be displayed on your Software page. Note that there is <em>no</em> space between <samp>-p</samp> and your password in the command above.</p>
<p>If successful, you'll see something similar to this:</p>
<pre><samp>Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 5
Server version: 5.5.55-0+deb8u1 (Debian)

Copyright (c) 2000, 2017, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql></samp></pre>

<h3 id="basic-ops">Basic operations</h3>
<p>Below is a list of basic operations you can perform from the MySQL monitor:</p>
<dl>
    <dt>List the databases</dt>
    <dd><kbd>show databases;</kbd></dd>
    <dt>Create a database</dt>
    <dd><kbd>CREATE DATABASE <var>dbname</var>;</kbd></dd>
    <dt>Select database as current</dt>
    <dd><kbd>USE <var>dbname</var></kbd></dd>
    <dt>Delete database</dt>
    <dd><kbd>DROP DATABASE <var>dbname</var>;</kbd></dd>
</dl>
<p>Replace <var>dbname</var> in the commands above with the database name. Database names are case sensitive.</p>
<p>Please note that the <samp>;</samp> character in the commands above is mandatory.</p>

<h3 id="backing-up">Backing up databases</h3>
<p>Backups are not created in the MySQL monitor - instead, you need to use another piece of software <samp>mysqldump</samp>. To do this, connect to your slot via SSH and use the following command (note that you'll need to use <em>your actual details</em>, rather than the variables here:</p>
<p><kbd>mysqldump --socket=<var>path-to-socket</var> -p<var>password</var> <var>dbname</var> &gt; ~/<var>dbname</var>.sql</kbd></p>
<p>Both the <var>path-to-socket</var> and <var>password</var> will be displayed on your Software page. Note that there is <em>no</em> space between <samp>-p</samp> and your password in the command above. Replace <var>dbname</var> with the name of the database you wish to back up.</p>
<p>To restore it, you can use the following (after changing the variables as above):</p>
<p><kbd>mysql --socket=<var>path-to-socket</var> -p<var>password</var> <var>dbname</var> &lt; ~/<var>dbname</var>.sql</kbd></p>
<p>If you need to restore a backup because the database no longer exists in your MySQL, please remake the database first using the commands in the Basic operations section above, then import the .sql file.</p>

<h2 id="uninstallation">Uninstalling</h2>
<aside class="alert note">The commands below will completely remove the software, config and databases - back up important data first!</aside>
<pre><kbd>pkill -9 -fu "$(whoami)" 'mysql'
rm -rf ~/private/mysql</kbd></pre>
<p>These commands act only on your slot, so the software page will not change to reflect the fact that MySQL has been removed. The socket, user and password will remain despite the fact that MySQL is gone.</p>

<h2 id="external-links">External links</h2>
<ul>
    <li><a href="https://dev.mysql.com/">MySQL homepage</a></li>
    <li><a href="https://dev.mysql.com/doc/">Documentation</a></li>
</ul>