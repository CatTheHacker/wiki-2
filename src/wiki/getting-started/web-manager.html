<h1>Feral web manager</h1>
<p>The Feral web manager is where a lot of the administration of your slot will take place. This article is intended to set out the different features of the web manager and provide links to individual pages which cover the features in more detail. No specialised knowledge is assumed.</p>

<details id="toc" open>
    <summary>Table of contents</summary>
    <nav>
        <ol>
            <li><a href="#logging-in">Logging in</a></li>
            <li><a href="#summary">Summary</a>
                <ul>
                    <li><a href="#renew">Renew slot</a></li>
                    <li><a href="#change-slot">Upgrade / change slot</a></li>
                    <li><a href="#data-transfer">Slot-to-slot data transfer</a></li>
                    <li><a href="#cancel">Cancel slot or subscription</a></li>
                </ul>
            </li>
            <li><a href="#software">Software</a></li>
            <li><a href="#install">Installing and reinstalling software</a></li>
            <li><a href="#account">Account</a>
                <ul>
                    <li><a href="#payments">Payments &amp; invoices</a></li>
                    <li><a href="#settings">Settings</a></li>
                </ul>
            </li>
            <li><a href="#support">Support</a></li>
            <li><a href="#reroute">Reroute traffic</a></li>
            <li><a href="#forward-feral">Forward Feral</a></li>
        </ol>
    </nav>
</details> 

<h2 id="logging-in">Logging in</h2>
<p>When you visit the <a href="https://www.feralhosting.com/login/">Feral login page</a> you'll need to input the e-mail address you signed up with and your password. If you have lost that password, please read the section on <a href="/wiki/slots/change-passwords#feral-pass">resetting your Feral account password</a>.</p>

<h2 id="summary">Summary</h2>
<p>When you first login you will see the details of your slot(s). The frame on the left-hand side of the page will list the slots you have together with links to the slot summary and software installation page. It will look like this:</p>
<img src="https://bitbucket.org/feralio/wiki/raw/HEAD/src/wiki/getting-started/web-manager/summary-frame.png" alt="the sidebar listing the slots a user has, showing an example user with three slots">
<p>The example user in the picture above has three slots. Underneath each one is a link to the Summary of the slot and the Software that has or can be installed on the slot via the web manager. The Software link is covered in a later section. If you click on the Summary link, you'll see, amongst other details, the following:</p>
<img src="https://bitbucket.org/feralio/wiki/raw/HEAD/src/wiki/getting-started/web-manager/summary-main.png" alt="the top section of the summary page, showing the user's username, server, bandwidth and disk usage">
<p>Your username and server will appear on this page in the format <samp>username@server</samp> - if you need this information for a ticket or to use with software, this is where you'd find it.</p>
<p>The summary page displays the following information:</p>
<dl>
    <dt>Renewal</dt>
    <dd>The monthly cost of the slot and the renewal date each month</dd>
    <dt>Standing</dt>
    <dd>Your balance and when the next renewal is due by</dd>
    <dt>Status</dt>
    <dd>Whether the slot is <samp>Active</samp> or <samp>Suspended</samp></dd>
    <dt>Disk usage</dt>
    <dd>Current disk usage on the slot with a button to refresh usage information</dd>
</dl>
<p>There is also a list of slot actions available</p>

<h3 id="renew">Renew Slot</h3>
<p>This is how you'd pay for the slot each month. You can choose a number of months to pay for - this number, multiplied by the cost of the slot, will be added to your balance on successful payment.</p>
<p>There's a separate guide on <a href="payment">payment problems</a> if you're having difficulties with payment.</p>

<h3 id="change-slot">Upgrade / change slot</h3>
<p>There is no way to provide a slot with increased (or decreased) storage capacity once it has been set up. In order to change storage capacity or to take advantage of offers related to storage capacity you will be required to purchase a new slot. You can start the upgrade process yourself at any time.</p>
<p>Below is our procedure to upgrade or change your slot:</p>
<ol>
    <li>Select and purchase a new slot from the list</li>
    <li>wait until you receive the slot (typically immediately, depending on if payment is received)</li>
    <li>(optional) transfer data to the new slot</li>
    <li>(optional) cancel your existing slot.</li>
</ol>
<p>The steps listed as <samp>(optional)</samp> do not have to be done if you do not want to. You can keep an older slot open if you wish - upgrading and changing does not by default remove the original slot. You can <a href="https://www.feralhosting.com/tickets/new">open a ticket</a> if you have any questions.</p>

<h3 id="data-transfer">Slot-to-slot data transfer</h3>
<p>We can copy all data on a slot to another, typically after an upgrade. Once the transfer has completed you'll be e-mailed confirmation of completion and instructions on how to get the data loaded in to your torrent client on the new slot. You should be aware that the process assumes you've kept the default data locations. If you haven't, you should consider either moving things back to default for the transfer, or copying across data manually and adding torrents one-by-one.</p>

<h3 id="cancel">Cancel slot or subscription</h3>
<p>You can cancel your slot by simply not renewing and it will be automatically recycled at the end of its period. However you may wish to consider reaching out for support from staff. We are always eager to try and rectify problems within our power to solve.</p>
<p>In addition to this, the following is available via <a href="https://www.feralhosting.com/tickets/new">a support ticket</a>:</p>
<ul>
    <li>new slots paid by card can be fully refunded in the first seven days</li>
    <li>prepaid card payments can be refunded (until the month begins)</li>
    <li>old slots can be cancelled (crediting remaining days) after data transfers</li>
</ul>

<h2 id="software">Software</h2>
<p>Clicking on the Software link will bring you to a page displaying all the software that you have installed via the web manager. Please note that if you are an older user who had to recover their account and already had software installed, you will not see any details for that software. The web manager cannot retroactively display these details. If you absolutely must have these details printed on your web manager, please reinstall the software using the instructions below.</p>
<p>Please also note that only the following may be installed via the web manager:</p>
<ul>
    <li>ruTorrent</li>
    <li>Deluge</li>
    <li>Transmission</li>
    <li>OpenVPN</li>
    <li>MySQL</li>
</ul>
<p>You should consult the area of this wiki which covers <a href="/wiki#software">installing custom software</a> for anything that isn't listed above.</p>

<h3 id="install">Installing and reinstalling software</h3>
<p>From the software page you can click on a link named <samp>install new software</samp>. As might be expected you can install new software here, but this is also the place to go if you wish to reinstall software.</p>
<p>In either case all you need to do is select the software from the list and click the button named <samp>Install Software</samp>. A command will then be sent to your server to perform the requested tasks and any access information printed to your software page.</p>
<p>The software page makes mention that users are welcome to install multiple clients - this means that you can have ruTorrent, Deluge and Transmission all running side-by-side if you wish.</p>
<p>An important point to remember is that reinstalling torrent clients will not remove torrents or their data. However, the configuration will be reset. This means any custom settings you had will be lost, including custom download locations. You will also need to restore or reinstall custom plugins.</p>

<h2 id="account">Account</h2>
<p>This section is where you need to go to look at your payment history, check your balance in more detail or change certain Feral account settings.</p>

<h3 id="payments">Payments &amp; invoices</h3>
<p>Each slot has a balance which is similar to a ledger. Associated payments and invoices increase and decrease the standing respectively.</p>

<h3 id="settings">Settings</h3>
<p>The following things can be changed:</p>
<dl>
    <dt>Payment Reminders</dt>
    <dd>The frequency at which payment reminder e-mails are sent to you.</dd>
    <dt>Automatic Renewal Day</dt>
    <dd>If you've set up a subscription, you can change how long before renewal day the payment is attempted each month.</dd>
</dl>

<h2 id="support">Support</h2>
<p>There are a number of ways you can get in touch with Feral staff and other Feral users. This section describes them briefly - how to properly use them will be explained in dedicated pages.</p>

<dl>
    <dt>IRC</dt>
    <dd>Service to communicate with other Feral users and get help with projects Feral is unable to support.</dd>
    <dt>Twitter</dt>
    <dd>A place for Feral to announce big infrastructural changes.</dd>
    <dt>Support Tickets</dt>
    <dd>The intended service for queries, problems and issues</dd>
    <dt>E-mail Support</dt>
    <dd>The support channel if you cannot open a ticket. Tickets are always preferred.</dd>
    <dt>Anything else</dt>
    <dd>Feral's owner's e-mail address. Should not be used for the vast majority of support queries.</dd>
</dl>
<p>You can see from the above that tickets are the intended way to get in touch with staff if you're experiencing issues. If you have a problem and do not know where to start, please see our guide on <a href="tickets">opening a ticket</a>.</p>

<h2 id="reroute">Reroute traffic</h2>
<p>This allows you to change the route data takes from Feral to your IP. This is useful if <a href="/wiki/slots/more-speed">speeds are low</a>.</p>

<h2 id="forward-feral">Forward Feral</h2>
<p>A page all about upgrades, improvements and changes that Feral are making or plan to make.</p>